#include"DZ30.4.h"
#include <map>

void task3() {
    std::string fName, argVal;
    //std::map <std::string, std::string> addArgs;
    std::vector <cpr::Pair> addingArgs;
    cpr::Response resp;

    while (fName != "get" && fName != "post") {
        std::cout << "Enter name of args or type of request(get/post)" << std::endl;
        std::cin >> fName;
        if (fName == "get" || fName == "post") break;
        std::cout << "Enter value of arg " << fName << " :" << std::endl;
        std::cin >> argVal;
        addingArgs.push_back(cpr::Pair(fName, argVal));
    }

    if (fName == "post") {
        resp = cpr::Post(cpr::Url("http://httpbin.org/post"),
                         cpr::Payload(addingArgs.begin(), addingArgs.end()));
    }
    else {
        std::string Url = "http://httpbin.org/get?";
        for (auto &it: addingArgs) Url += it.key + "=" + it.value + "&";

        resp = cpr::Get(cpr::Url(Url));
    }
    std::cout << resp.text <<std::endl;
}