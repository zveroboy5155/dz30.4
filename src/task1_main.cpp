#include "DZ30.4.h"


void task1(){
    //get”, “post”, “put”, “delete”, “patch”.
    std::string cmd;

    cpr::Response resp;

    std::cout << "Введите комманду(get, post, put, delete, patch)" << std::endl;
    std::cin >> cmd;

    if(cmd == "get") resp = cpr::Get(cpr::Url("http://httpbin.org/get"));
    else if(cmd == "post") resp = cpr::Post(cpr::Url("http://httpbin.org/post"));
    else if(cmd == "put") resp = cpr::Put(cpr::Url("http://httpbin.org/put"));
    else if(cmd == "delete") resp = cpr::Delete(cpr::Url("http://httpbin.org/delete"));
    else if(cmd == "patch") resp = cpr::Patch(cpr::Url("http://httpbin.org/patch"));
    std::cout << resp.text << std::endl;
}