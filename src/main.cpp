#include <iostream>
#include <DZ30.4.h>

int main() {
	int cmd = -1;

	while (cmd != 0) {
		std::cout << "Enter task number( 1 - 3) or 0 for exit" << std::endl;
		std::cin >> cmd;
        switch (cmd) {
            case 1:
                task1();
                break;
            case 2:
                task2();
                break;
            case 3:
                task3();
                break;
            default:
                break;
        }
	}
    
    return 0;
}
